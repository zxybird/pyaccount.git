#!/bin/bash

rm -rf dist
rm -rf build
rm -rf *.egg-info
python setup.py bdist_wheel --universal
#python setup.py install
twine upload dist/pyaccount*
