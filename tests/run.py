#!/usr/bin/python
# -*- coding: UTF-8 -*-
# author : bird.zhang@ximalaya.com

import pyaccount.pyaccount as pyaccount


def test_init():
    parser = pyaccount.gen_parse()
    args = parser.parse_args('-p passwd init 1 2 3'.split())
    pyaccount.init(args)


def test_add():
    parser = pyaccount.gen_parse()
    args = parser.parse_args('-p passwd add www.baidu.com zxybird ZXY1992@baidu'.split())
    pyaccount.add(args)


def test_delete():
    parser = pyaccount.gen_parse()
    args = parser.parse_args('-p passwd delete 1'.split())
    pyaccount.delete(args)


def test_find():
    parser = pyaccount.gen_parse()
    args = parser.parse_args('-p passwd find '.split())
    pyaccount.find(args)


test_init()
test_add()
test_find()
# test_delete()
