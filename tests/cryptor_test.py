#!/usr/bin/python
# -*- coding: UTF-8 -*-
# author : bird.zhang@ximalaya.com

from pyaccount.cryptor import *
from pyaccount import pyaccount


def refactor_key_test():
    print(refactor_key(1111111))


@check_auth_decorator
def test(args):
    pc = Cryptor('粉刷匠1231繁體testひらahdghkkhhgoiijjaslkjgafkjgljlk;djhjj;ljlj')  # 初始化密钥
    e = pc.encrypt("my book is free")
    d = pc.decrypt(e)
    print((e, d))
    e = pc.encrypt("我是一个粉刷匠1231繁體testひらがな")
    d = pc.decrypt(e)
    print((e, d))


# print(check_auth('/data/.pyaccount/.message', '/data/.pyaccount/.ciphertext', 'passwd'))


# refactor_key_test()
parser = pyaccount.gen_parse()
args = parser.parse_args('-p passwd add www.baidu.com zxybird ZXY1992@baidu'.split())
test(args)

print(Cryptor(0).version)
