# pyaccount

#### 项目介绍

一个用来存储账号密码的python脚本

1Password感觉太复杂，感觉不太会用，所以就想着自己写个。

结合pwgen或者mkpaswd更好哦

#### 软件架构

使用py3写的一个简单的cli

主要为了方便自己学习：

* python3基础的enum、class、module、package
* python3的decorator
* argprase、click
* pandas
* base64、Crypto


#### 安装教程

`pip install pyaccount`

#### 使用说明

* pyaccount init -p 123456
* pyaccount -p 123456 find
* pyaccount -p 123456 find gitee
* pyaccount -p 123456 add gitee.zxybird username passwd
* pyaccount -p 123456 del gitee.zxybird
* pyaccount -p 123456 mdf gitee.zxybird username passwd

#### ToDoList

* ~~项目模块化，项目的目录需更加分明~~
* ~~添加测试~~
* argprase换成click
* 数据文件从固定位置进个人目录
* ~~抽出常量和配置，配置走文件配置~~
* 融合pwgen或者mkpaswd

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)