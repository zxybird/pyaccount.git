# -*- encoding: UTF-8 -*-
from setuptools import setup, find_packages

"""
打包的用的setup必须引入，
"""

VERSION = '0.0.4'

setup(name='pyaccount',
      version=VERSION,
      description="a tiny and smart cli to store account and passwd",
      long_description='just for fun',
      classifiers=[],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      # keywords='python account password terminal',
      author='Bird.Zhang',
      author_email='476100281@qq.com',
      url='https://gitee.com/zxybird/pyaccount.git',
      license='MIT',
      packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
      # include_package_data=True,
      zip_safe=False,
      install_requires=[
          'argparse',
          'pandas',
          'base64',
          'Crypto',
      ],
      entry_points={
          'console_scripts': [
              'pyaccount = pyaccount.pyaccount:run'
          ]
      },
      )

# install_requires=[
# 'requests',
# 'pycookiecheat'
# ] + (['pyobjc-core', 'pyobjc'] if 'darwin' in sys.platform else []),
