#!/usr/bin/python
# -*- coding: UTF-8 -*-
# author : bird.zhang@ximalaya.com

import os


def file_existed(file_path):
    """
    判断文件是否存在，通过isfile()来判断，避免direction当作文件
    """
    return os.path.isfile(file_path)


def assert_file_exist(file_path):
    """
    判断文件是否存在，通过isfile()来判断，避免direction当作文件
    """
    if not os.path.isfile(file_path):
        raise FileNotFoundError


def assert_file_not_exist(file_path):
    """
    判断文件是否存在，通过isfile()来判断，避免direction当作文件
    """
    if os.path.isfile(file_path):
        raise FileExistsError
