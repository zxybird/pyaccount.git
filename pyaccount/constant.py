#!/usr/bin/python
# -*- coding: UTF-8 -*-
# author : bird.zhang@ximalaya.com

from enum import Enum

CHARSET_UTF8 = 'utf-8'

PYACCOUNT_HOME = '/data/.pyaccount/'
STORE_DATA_FILE_NAME = 'store.data'
ENCODE_STR_FILE_NAME = '.ciphertext'
DECODE_STR_FILE_NAME = '.message'

STORE_DATA_FILE_PATH = PYACCOUNT_HOME + STORE_DATA_FILE_NAME
ENCODE_STR_FILE_PATH = PYACCOUNT_HOME + ENCODE_STR_FILE_NAME
DECODE_STR_FILE_PATH = PYACCOUNT_HOME + DECODE_STR_FILE_NAME

ADDRESS = 'address'
USERNAME = 'username'
PASSWORD = 'password'


class COMMAND(Enum):
    INIT = 'init'
    ADD = 'add'
    DELETE = 'delete'
    MODIFY = 'modify'
    FIND = 'find'
    UNKNOWN = 'unknown'
