#!/usr/bin/python
# -*- coding: UTF-8 -*-
# author : bird.zhang@ximalaya.com

import argparse


def gen_parse():
    parser = argparse.ArgumentParser(description='A simple cli to store account info.')
    parser.add_argument('-p', '--password', metavar='', dest='password', help="your password to decode data file.",
                        required=True)
    parser.add_argument('-D', '--debug', dest='debug', action='store_true', help='whether show debug info.')
    parser.add_argument('command', help="command:init|add|delete|find")
    parser.add_argument('args', nargs='*', help="the command`s args")
    return parser
